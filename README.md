# ECR MODULE #

## Install terraform  ##

run the commands below to install terraform 

```sh
wget https://releases.hashicorp.com/terraform/1.0.3/terraform_1.0.3_linux_amd64.zip
```

```sh
unzip terraform_1.0.3_linux_amd64.zip
```

```sh
sudo mv terraform /usr/bin/ && terraform --version
```

## Steps for using the module  ##

* Clone Repository
* fill in the .tfvars file 
* run terraform init
* run terraform apply --var-file=ecr.tfvars

