terraform {
  backend "s3" {
    bucket         = "ecr-terraform-states-bucket"
    key            = "infraestrutura/ecr.tfstate"
    region         = "us-east-1"
    dynamodb_table = "ecr-terraform-state-lock-table"
    profile        = "env-tst"
  }
}
