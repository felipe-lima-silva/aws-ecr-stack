variable "name" {
  description = "Nome do Registry"
  type        = string
}

variable "encription" {
  description = "Defini se o Repo será encriptado ou nao"
  type        = string
}

variable "scan_push" {
  description = "Scan e Push"
  type        = bool
}

variable "tag_mutability" {
  description = "Tag Mutability"
  type        = string
}

variable "name_registry" {
  description = "Lista com o nome dos Repositorios"
  type        = list(string)
  default     = []
}
