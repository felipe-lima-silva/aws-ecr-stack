resource "aws_ecr_repository" "application_registry" {

  count = length(var.name_registry)
  name  = var.name_registry[count.index]


  image_tag_mutability = var.tag_mutability

  encryption_configuration {
    encryption_type = var.encription
  }

  image_scanning_configuration {
    scan_on_push = var.scan_push
  }
}
