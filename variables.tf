variable "region" {
  description = "Região do ambiente"
  type        = string
}

variable "environment" {
  description = "Ambiente do projeto."
  type        = string
  default     = "tst"
}

variable "profile_preffix" {
  type        = string
  sensitive   = true
  description = "Prefixo do AWS profile. Padrão $provile_preffix_$environment"
  default     = "env-tst"
}

variable "account_id" {
  type        = string
  description = "Account ID do ambiente."
  sensitive   = true
  default     = "XXXXXXXXXXX"
}

variable "tags" {
  type        = map(string)
  description = "Tags para todos os recursos do ambiente."
  default     = {}
}
